<?php
$url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . getenv('HTTP_HOST');
?>

<!DOCTYPE html>
<html lang="en-US">
<!-- Mirrored from singlestroke.io/demo/jackrose-wp/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 11 Mar 2021 09:57:22 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="Hong & Tuan's house">
    <meta property="og:image" content="<?php echo $url ?>/wp-content/images/image-title.jpg">
    <meta property="og:description" content="Nhà không cần quá lớn, miễn là trong đó có đủ tình yêu thương.">
    <link rel="icon" type="image/png" href="wp-content/uploads/2015/11/header-logo.png"/>

    <link media="all" href="wp-content/cache/autoptimize/css/autoptimize_d741e3f8f3767abc4c53476109f897f7.css"
          rel="stylesheet"/>
    <title>Rose & Tude - Ours story</title>
    <script type="text/javascript">paceOptions = {
            target: '#preloader .preloader-content',
        }</script>
    <link rel='dns-prefetch' href='http://fonts.googleapis.com/'/>
    <link rel='dns-prefetch' href='http://s.w.org/'/>
    <link rel="alternate" type="application/rss+xml" title="Jack &amp; Rose &raquo; Feed" href="feed/index.html"/>
    <link rel="alternate" type="application/rss+xml" title="Jack &amp; Rose &raquo; Comments Feed"
          href="comments/feed/index.html"/>
    <style type="text/css">img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }</style>
    <link rel='stylesheet' id='google-fonts-css'
          href='http://fonts.googleapis.com/css?family=Alex+Brush%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;subset=latin&amp;ver=5.5.3'
          type='text/css' media='all'/>
    <style id='jackrose-inline-css' type='text/css'>body {
            font-family: Georgia, Times, "Times New Roman", serif;
        }

        .typography-menu,
        .button,
        button,
        input[type="button"],
        input[type="reset"],
        input[type="submit"],
        .header-section,
        .nav-links,
        #cancel-comment-reply-link {
            font-family: Georgia, Times, "Times New Roman", serif;
        }

        .typography-section-heading {
            font-family: "Alex Brush";
        }

        .typography-title,
        .typography-heading,
        h1, h2, h3, h4, h5, h6,
        .comment-reply-title,
        .comments-title,
        #gwolle_gb_new_entry h3 {
            font-family: Georgia, Times, "Times New Roman", serif;
        }</style>
<!--    <script type='text/javascript' id='monsterinsights-frontend-script-js-extra'>var monsterinsights_frontend = {-->
<!--            "js_events_tracking": "true",-->
<!--            "download_extensions": "doc,pdf,ppt,zip,xls,docx,pptx,xlsx",-->
<!--            "inbound_paths": "[]",-->
<!--            "home_url": "https:\/\/singlestroke.io\/demo\/jackrose-wp",-->
<!--            "hash_tracking": "false"-->
<!--        };</script>-->
    <link rel="https://api.w.org/" href="wp-json/index.html"/>
    <link rel="alternate" type="application/json" href="wp-json/wp/v2/pages/7.json"/>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd"/>
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml"/>
    <meta name="generator" content="WordPress 5.5.3"/>
    <link rel="canonical" href="index.html"/>
    <link rel='shortlink' href='index.html'/>
    <style type="text/css" media="all"
           id="siteorigin-panels-layouts-head">/* Layout 7 */
        #pgc-7-0-0, #pgc-7-1-0, #pgc-7-2-0, #pgc-7-3-0, #pgc-7-4-0, #pgc-7-5-0, #pgc-7-7-0, #pgc-7-8-0, #pgc-7-9-0, #pgc-7-10-0 {
            width: 100%;
            width: calc(100% - (0 * 30px))
        }

        #pg-7-0, #pg-7-1, #pg-7-2, #pg-7-3, #pg-7-4, #pg-7-5, #pg-7-6, #pg-7-7, #pg-7-8, #pg-7-9, #pg-7-10, #pl-7 .so-panel:last-child {
            margin-bottom: 0px
        }

        #pgc-7-6-0, #pgc-7-6-2 {
            width: 20%;
            width: calc(20% - (0.8 * 30px))
        }

        #pgc-7-6-1 {
            width: 60%;
            width: calc(60% - (0.4 * 30px))
        }

        #pl-7 .so-panel {
            margin-bottom: 30px
        }

        #pg-7-0 > .panel-row-style, #pg-7-2 > .panel-row-style, #pg-7-5 > .panel-row-style, #pg-7-7 > .panel-row-style, #pg-7-9 > .panel-row-style {
            padding: 100px 0px
        }

        #panel-7-0-0-0 > .panel-widget-style {
            margin-bottom: 2em
        }

        #pg-7-1 > .panel-row-style {
            background-color: #ecf2f0;
            padding: 60px 0px
        }

        #pg-7-3 > .panel-row-style, #pg-7-10 > .panel-row-style {
            background-color: #f6f4f2;
            padding: 100px 0px
        }

        #pg-7-4 > .panel-row-style {
            background-color: #f6f4f2;
            padding: 0px
        }

        #pg-7-6 > .panel-row-style {
            background-color: #b4d2c8;
            background-image: url(wp-content/uploads/2015/11/rsvp.jpg);
            padding: 60px 0px
        }

        #panel-7-6-1-0 > .panel-widget-style {
            background-color: #ffffff;
            padding: 3em;
            margin-bottom: -30px
        }

        #panel-7-6-1-1 > .panel-widget-style {
            background-color: #ffffff;
            padding: 0em 3em 3em
        }

        #panel-7-7-0-2 > .panel-widget-style {
            margin-top: 8em
        }

        #pg-7-8 > .panel-row-style {
            background-color: #c3bbab;
            background-image: url(wp-content/images/quote.jpg);
            padding: 90px 0px 30px
        }

        #panel-7-8-0-0 > .panel-widget-style {
            color: #ffffff
        }

        @media (max-width: 767px) {
            #pg-7-0.panel-no-style, #pg-7-0.panel-has-style > .panel-row-style, #pg-7-1.panel-no-style, #pg-7-1.panel-has-style > .panel-row-style, #pg-7-2.panel-no-style, #pg-7-2.panel-has-style > .panel-row-style, #pg-7-3.panel-no-style, #pg-7-3.panel-has-style > .panel-row-style, #pg-7-4.panel-no-style, #pg-7-4.panel-has-style > .panel-row-style, #pg-7-5.panel-no-style, #pg-7-5.panel-has-style > .panel-row-style, #pg-7-6.panel-no-style, #pg-7-6.panel-has-style > .panel-row-style, #pg-7-7.panel-no-style, #pg-7-7.panel-has-style > .panel-row-style, #pg-7-8.panel-no-style, #pg-7-8.panel-has-style > .panel-row-style, #pg-7-9.panel-no-style, #pg-7-9.panel-has-style > .panel-row-style, #pg-7-10.panel-no-style, #pg-7-10.panel-has-style > .panel-row-style {
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column
            }

            #pg-7-0 > .panel-grid-cell, #pg-7-0 > .panel-row-style > .panel-grid-cell, #pg-7-1 > .panel-grid-cell, #pg-7-1 > .panel-row-style > .panel-grid-cell, #pg-7-2 > .panel-grid-cell, #pg-7-2 > .panel-row-style > .panel-grid-cell, #pg-7-3 > .panel-grid-cell, #pg-7-3 > .panel-row-style > .panel-grid-cell, #pg-7-4 > .panel-grid-cell, #pg-7-4 > .panel-row-style > .panel-grid-cell, #pg-7-5 > .panel-grid-cell, #pg-7-5 > .panel-row-style > .panel-grid-cell, #pg-7-6 > .panel-grid-cell, #pg-7-6 > .panel-row-style > .panel-grid-cell, #pg-7-7 > .panel-grid-cell, #pg-7-7 > .panel-row-style > .panel-grid-cell, #pg-7-8 > .panel-grid-cell, #pg-7-8 > .panel-row-style > .panel-grid-cell, #pg-7-9 > .panel-grid-cell, #pg-7-9 > .panel-row-style > .panel-grid-cell, #pg-7-10 > .panel-grid-cell, #pg-7-10 > .panel-row-style > .panel-grid-cell {
                width: 100%;
                margin-right: 0
            }

            #pgc-7-6-0, #pgc-7-6-1 {
                margin-bottom: 30px
            }

            #pl-7 .panel-grid-cell {
                padding: 0
            }

            #pl-7 .panel-grid .panel-grid-cell-empty {
                display: none
            }

            #pl-7 .panel-grid .panel-grid-cell-mobile-last {
                margin-bottom: 0px
            }
        }</style>
    <style id="kirki-inline-styles">#preloader, .pace-progress {
            background-color: #b4d2c8;
        }

        a {
            color: #b4d2c8;
        }

        .jackrose-sow-gallery-grid-filters a.active {
            border-color: #b4d2c8;
        }

        .button, button, input[type="button"], input[type="reset"], input[type="submit"] {
            background-color: #b4d2c8;
            border-color: #b4d2c8;
            color: #ffffff;
        }

        .button:hover, button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, .button:focus, button:focus, input[type="button"]:focus, input[type="reset"]:focus, input[type="submit"]:focus, .button:active, button:active, input[type="button"]:active, input[type="reset"]:active, input[type="submit"]:active {
            background-color: #bcd7ce;
            border-color: #bcd7ce;
            color: #ffffff;
        }

        .header-section, .header-navigation ul ul {
            background-color: #ffffff;
        }

        .header-section, .header-navigation ul ul, .header-navigation ul ul li {
            border-color: #e5e5e5;
        }

        .header-section, .header-section a, .header-navigation-toggle {
            color: #888888;
        }

        .header-section a:hover, .header-section a:focus, .header-navigation-toggle:hover, .header-navigation-toggle:focus {
            color: #444444;
        }

        .header-navigation > div > ul > li > a:hover:after, .header-navigation > div > ul > li > a.focus:after {
            background-color: #b4d2c8;
        }

        .header-section .ribbon-menu, .header-section .ribbon-menu:hover, .header-section .ribbon-menu:focus {
            background-color: #b4d2c8;
            color: #ffffff;
        }

        .header-section .ribbon-menu:after {
            border-color: #b4d2c8;
        }

        .sakura.hero-effect-item {
            background: #ffffff;
        }

        .hero-button, .hero-button:hover, .hero-button:focus {
            color: #ffffff;
        }

        .footer-section {
            padding: 15% 0 3em;
            background-color: #ffffff;
            background-image: url(wp-content/uploads/2015/11/footer.jpg);
            background-repeat: no-repeat;
            background-position: center bottom;
            color: #e5e5e5;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -ms-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .footer-section a, .footer-section a:hover, .footer-section a:focus {
            color: #ffffff;
        }

        @media screen and ( max-width: 1023px ) {
            .header-navigation > div {
                background-color: #ffffff;
            }

            .header-navigation > div > ul, .header-navigation li {
                border-color: #e5e5e5;
            }
        }</style>
</head>
<body class="home page-template page-template-page-templates page-template-one-page page-template-page-templatesone-page-php page page-id-7 jackrose-enable-animations siteorigin-panels siteorigin-panels-before-js siteorigin-panels-home">
<div id="preloader" class="preloader">
    <div class="wrapper">
        <div class="preloader-content"><img
                    src="wp-content/uploads/2015/11/hero-logo.png"
                    width="400" height="400" alt=""></div>
    </div>
</div>
<div id="page" class="hfeed site"><a class="skip-link screen-reader-text" href="#content">Skip to content</a>
    <div id="top"></div>
    <div id="hero" class="hero-section">
        <div class="hero-slider" data-jackrose-autoplay="3000">
            <div id="hero-slide-0" class="hero-slide">
                <div class="hero-slide-background section-background"><img class="image-bg" alt=""
                                                                           src="wp-content/uploads/2015/11/hero3.jpg"
                                                                           width="1600"
                                                                           height="900"
                                                                           data-stellar-ratio="0.5"></div>
                <div class="hero-slide-overlay" style="background-color: rgba(130,160,150,0.3)"></div>
            </div>
            <div id="hero-slide-1" class="hero-slide">
                <div class="hero-slide-background section-background"><img class="image-bg" alt=""
                                                                           src="wp-content/uploads/2015/11/hero2.jpg"
                                                                           width="1600"
                                                                           height="900"
                                                                           data-stellar-ratio="0.5"></div>
                <div class="hero-slide-overlay" style="background-color: rgba(130,160,150,0.3)"></div>
            </div>
            <div id="hero-slide-2" class="hero-slide">
                <div class="hero-slide-background section-background"><img class="image-bg" alt=""
                                                                           src="wp-content/uploads/2015/11/hero1.jpg"
                                                                           width="1600"
                                                                           height="900"
                                                                           data-stellar-ratio="0.5"></div>
                <div class="hero-slide-overlay" style="background-color: rgba(130,160,150,0.3)"></div>
            </div>
        </div>
        <div id="hero-effect" class="hero-effect" data-jackrose-effect="petal"></div>
        <div id="hero-logo" class="hero-logo">
            <div class="wrapper">
                <div class="hero-logo-image"><img width="400" height="400"
                                                  src="wp-content/uploads/2015/11/hero-logo.png"
                                                  class="attachment-full size-full" alt="Jack &amp; Rose" loading="lazy"
                                                  srcset="wp-content/uploads/2015/11/hero-logo.png 400w, wp-content/uploads/2015/11/hero-logo.png 150w, wp-content/uploads/2015/11/hero-logo.png 300w"
                                                  sizes="(max-width: 400px) 100vw, 400px"/></div>
                <div class="hero-action"><a href="#intro" class="hero-button anchor-link"><span>Enter Site</span><i
                                class="fa fa-angle-double-down"></i></a></div>
            </div>
        </div>
    </div>
    <div id="header" class="header-anchor header-anchor-bottom">
        <header id="masthead" class="header-section site-header header-floating" role="banner">
            <div class="wrapper"><h1 class="header-logo site-branding"><a href="index.html" rel="home"> <img width="50"
                                                                                                             height="50"
                                                                                                             src="wp-content/uploads/2015/11/header-logo.png"
                                                                                                             class="attachment-full size-full"
                                                                                                             alt="Jack &amp; Rose"
                                                                                                             loading="lazy"/>
                    </a></h1> <a class="ribbon-menu anchor-link" href="index.html#rsvp"> <span>H & T</span> </a>
                <nav id="site-navigation" class="header-navigation main-navigation clear" role="navigation">
                    <button class="header-navigation-toggle menu-toggle toggle"><span class="fa fa-navicon"></span>
                        <span class="screen-reader-text">Primary Menu</span></button>
                    <div class="menu-primary-container">
                        <ul id="primary-menu" class="menu">
                            <li id="menu-item-45"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-45">
                                <a href="#">Home</a>
                            </li>
                            <li id="menu-item-49"
                                class="anchor-link menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-49">
                                <a href="index.html#groom-bride" aria-current="page">Groom &#038; Bride</a></li>
                            <li id="menu-item-48"
                                class="anchor-link menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-48">
                                <a href="index.html#events" aria-current="page">When &#038; Where</a></li>
                            <li id="menu-item-50"
                                class="anchor-link menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-50">
                                <a href="index.html#gallery" aria-current="page">Gallery</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
    </div>
    <div id="content" class="site-content content-section">
        <div class="wrapper">
            <div id="primary" class="content-area no-sidebar">
                <main id="main" class="site-main" role="main">
                    <article id="post-7" class="post-7 page type-page status-publish hentry">
                        <div class="entry-content">
                            <div id="pl-7" class="panel-layout">
                                <div id="pg-7-0" class="panel-grid panel-has-style">
                                    <div id="intro" data-jackrose-anchor="intro"
                                         class="panel-row-style panel-row-style-for-7-0">
                                        <div id="pgc-7-0-0" class="panel-grid-cell">
                                            <div id="panel-7-0-0-0"
                                                 class="so-panel widget widget_jackrose-sow-couple-intro panel-first-child"
                                                 data-index="0">
                                                <div class="jackrose-animation-fade-in panel-widget-style panel-widget-style-for-7-0-0-0">
                                                    <div class="so-widget-jackrose-sow-couple-intro so-widget-jackrose-sow-couple-intro-default-d75171398898">
                                                        <div class="jackrose-sow-couple-intro align-center">
                                                            <div class="jackrose-sow-couple-intro-groom ">
                                                                <div class="jackrose-sow-couple-intro-photo"><img
                                                                            width="240" height="260"
                                                                            src="wp-content/uploads/2015/11/intro-groom.png"
                                                                            class="attachment-full size-full" alt=""
                                                                            loading="lazy"/></div>
                                                                <div class="jackrose-sow-couple-intro-name typography-heading">
                                                                    Viết Tuấn
                                                                </div>
                                                            </div>
                                                            <div class="jackrose-sow-couple-intro-separator "><img
                                                                        width="160" height="100"
                                                                        src="wp-content/uploads/2015/11/intro-separator.png"
                                                                        class="attachment-full size-full" alt=""
                                                                        loading="lazy"/></div>
                                                            <div class="jackrose-sow-couple-intro-bride ">
                                                                <div class="jackrose-sow-couple-intro-photo"><img
                                                                            width="240" height="260"
                                                                            src="wp-content/uploads/2015/11/intro-bride.png"
                                                                            class="attachment-full size-full" alt=""
                                                                            loading="lazy"/></div>
                                                                <div class="jackrose-sow-couple-intro-name typography-heading">
                                                                    Hồng Trần
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="panel-7-0-0-1"
                                                 class="so-panel widget widget_jackrose-sow-section-heading panel-last-child"
                                                 data-index="1">
                                                <div class="jackrose-animation-fade-in panel-widget-style panel-widget-style-for-7-0-0-1">
                                                    <div class="so-widget-jackrose-sow-section-heading so-widget-jackrose-sow-section-heading-default-d75171398898">
                                                        <header class="jackrose-sow-section-header align-center">
                                                            <p class="jackrose-sow-section-subheading"
                                                               style="color: #888888">"Đức mến tha thứ tất cả, tin tưởng
                                                                tất cả, hy vọng tất cả" - Côrintô 13,1-8</p>
                                                            <h2
                                                                    class="jackrose-sow-section-heading typography-section-heading"
                                                                    style="color: #dcc8b4">Are getting married</h2>

                                                            <p class="jackrose-sow-section-subheading"
                                                               style="color: #272727">11/04/2021 - Bao Loc City</p>
                                                        </header>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="pg-7-1" class="panel-grid panel-has-style">
                                    <div class="siteorigin-panels-stretch panel-row-style panel-row-style-for-7-1"
                                         data-stretch-type="full" id="countdown" data-jackrose-anchor="countdown">
                                        <div id="pgc-7-1-0" class="panel-grid-cell">
                                            <div id="panel-7-1-0-0"
                                                 class="so-panel widget widget_jackrose-sow-countdown panel-first-child panel-last-child"
                                                 data-index="2">
                                                <div class="jackrose-animation-fade-in panel-widget-style panel-widget-style-for-7-1-0-0">
                                                    <div class="so-widget-jackrose-sow-countdown so-widget-jackrose-sow-countdown-default-d75171398898">
                                                        <div class="jackrose-sow-countdown align-center"
                                                             data-jackrose-target="2021/04/11 11:00:00"
                                                             data-jackrose-build="m_n_H_M_S">
                                                            <!--                                                            <div class="jackrose-sow-countdown-fragment jackrose-sow-countdown-months"-->
                                                            <!--                                                                 data-jackrose-format="m"-->
                                                            <!--                                                                 data-jackrose-singular="month"-->
                                                            <!--                                                                 data-jackrose-plural="months">-->
                                                            <!--                                                                <big></big><small></small></div>-->
                                                            <div class="jackrose-sow-countdown-fragment jackrose-sow-countdown-days"
                                                                 data-jackrose-format="n"
                                                                 data-jackrose-singular="day"
                                                                 data-jackrose-plural="days"><big></big><small></small>
                                                            </div>
                                                            <div class="jackrose-sow-countdown-fragment jackrose-sow-countdown-hours"
                                                                 data-jackrose-format="H"
                                                                 data-jackrose-singular="hour"
                                                                 data-jackrose-plural="hours"><big></big><small></small>
                                                            </div>
                                                            <div class="jackrose-sow-countdown-fragment jackrose-sow-countdown-minutes"
                                                                 data-jackrose-format="M"
                                                                 data-jackrose-singular="minute"
                                                                 data-jackrose-plural="minutes">
                                                                <big></big><small></small></div>
                                                            <div class="jackrose-sow-countdown-fragment jackrose-sow-countdown-seconds"
                                                                 data-jackrose-format="S"
                                                                 data-jackrose-singular="second"
                                                                 data-jackrose-plural="seconds">
                                                                <big></big><small></small></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="pg-7-2" class="panel-grid panel-has-style">
                                    <div id="groom-bride" data-jackrose-anchor="groom-bride"
                                         class="panel-row-style panel-row-style-for-7-2">
                                        <div id="pgc-7-2-0" class="panel-grid-cell">
                                            <div id="panel-7-2-0-0"
                                                 class="so-panel widget widget_jackrose-sow-section-heading panel-first-child"
                                                 data-index="3">
                                                <div class="jackrose-animation-fade-in panel-widget-style panel-widget-style-for-7-2-0-0">
                                                    <div class="so-widget-jackrose-sow-section-heading so-widget-jackrose-sow-section-heading-default-d75171398898">
                                                        <header class="jackrose-sow-section-header align-center"><h2
                                                                    class="jackrose-sow-section-heading typography-section-heading"
                                                                    style="color: #dcc8b4">Groom &amp; Bride</h2>
                                                            <p class="jackrose-sow-section-subheading"
                                                               style="color: #888888"></p></header>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="panel-7-2-0-1"
                                                 class="so-panel widget widget_jackrose-sow-about-us-grid panel-last-child"
                                                 data-index="4">
                                                <div class="so-widget-jackrose-sow-about-us-grid so-widget-jackrose-sow-about-us-grid-default-d75171398898">
                                                    <div class="jackrose-sow-about-us-grid">
                                                        <div class="jackrose-sow-about-us-grid-item jackrose-sow-about-us-grid-item-photo-left clear jackrose-animation-fade-in-up"
                                                             style="background-color: #f6f4f2">
                                                            <div class="jackrose-sow-about-us-grid-item-photo">
                                                                <div class="jackrose-sow-about-us-grid-item-photo-image"
                                                                     style="background-image: url(wp-content/uploads/2015/11/about-groom-540x405.jpg);"></div>
                                                            </div>
                                                            <div class="jackrose-sow-about-us-grid-item-text"><h4
                                                                        class="jackrose-sow-about-us-grid-item-name typography-heading">
                                                                    VIẾT TUẤN</h4>
                                                                <div class="jackrose-sow-about-us-grid-item-content"><p>
                                                                        Anh có 2 điều may mắn nhất từ trước tới giờ. Thứ
                                                                        nhất là được làm con của bố mẹ.
                                                                        Đứa con được dạy bởi cuộc sống lao động cần cù,
                                                                        nên khi lớn lên luôn biết trân trọng mọi thứ
                                                                        mình đang có.
                                                                        <br>Và khi trưởng thành, gặp được Em là điều may
                                                                        mắn thứ 2 của anh.
                                                                        Em tiếp thêm động lực cho anh lao động và phấn
                                                                        đấu từng ngày. Anh tin chúng ta sẽ xây dựng nên
                                                                        một cuộc sống gia đình lành mạnh và hạnh phúc.
                                                                    </p></div>
                                                                <div class="jackrose-sow-about-us-grid-item-links"><a
                                                                            href="https://www.facebook.com/viet.tuan.9210/">
                                                                        <span class="fa fa-facebook"></span>
                                                                        <span class="screen-reader-text">facebook</span></a>
                                                                    <a href="https://www.instagram.com/viet.tuann/">
                                                                        <span class="fa fa-instagram"></span>
                                                                        <span class="screen-reader-text">instagram</span>
                                                                    </a></div>
                                                            </div>
                                                        </div>
                                                        <div class="jackrose-sow-about-us-grid-item jackrose-sow-about-us-grid-item-photo-right clear jackrose-animation-fade-in-up"
                                                             singlestroke.io
                                                        /demo
                                                        style="background-color: #f6f4f2">
                                                        <div class="jackrose-sow-about-us-grid-item-photo">
                                                            <div class="jackrose-sow-about-us-grid-item-photo-image"
                                                                 style="background-image: url(wp-content/uploads/2015/11/about-bride-540x405.jpg);"></div>
                                                        </div>
                                                        <div class="jackrose-sow-about-us-grid-item-text"><h4
                                                                    class="jackrose-sow-about-us-grid-item-name typography-heading">
                                                                HỒNG TRẦN</h4>
                                                            <div class="jackrose-sow-about-us-grid-item-content">
                                                                <p>
                                                                    Cô bé có đôi mắt biết nói, chính vì vậy mà em rất
                                                                    kiệm lời và chân thành:)) . Em đặt nhiều tình cảm, niềm tin và hy vọng cho người mình chọn lựa.
                                                                    <br>
                                                                    Sống là cho đâu chỉ nhận riêng mình - Trao đi yêu
                                                                    thương nhận lại yêu thương
                                                                </p>
                                                            </div>
                                                            <div class="jackrose-sow-about-us-grid-item-links">
                                                                <a href="https://www.facebook.com/kimhong198">
                                                                    <span class="fa fa-facebook"></span> <span
                                                                            class="screen-reader-text">facebook</span>
                                                                </a>
                                                                <a href="https://www.instagram.com/hongtran___/"> <span
                                                                            class="fa fa-instagram"></span>
                                                                    <span class="screen-reader-text">instagram</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="pg-7-3" class="panel-grid panel-has-style">
                                <div class="siteorigin-panels-stretch panel-row-style panel-row-style-for-7-3"
                                     data-stretch-type="full" id="events" data-jackrose-anchor="events">
                                    <div id="pgc-7-3-0" class="panel-grid-cell">
                                        <div id="panel-7-3-0-0"
                                             class="so-panel widget widget_jackrose-sow-section-heading panel-first-child"
                                             data-index="5">
                                            <div class="jackrose-animation-fade-in panel-widget-style panel-widget-style-for-7-3-0-0">
                                                <div class="so-widget-jackrose-sow-section-heading so-widget-jackrose-sow-section-heading-default-d75171398898">
                                                    <header class="jackrose-sow-section-header align-center"><h2
                                                                class="jackrose-sow-section-heading typography-section-heading"
                                                                style="color: #c8b4a0">When &amp; Where</h2>
                                                        <p class="jackrose-sow-section-subheading"
                                                           style="color: #888888"></p></header>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="panel-7-3-0-1"
                                             class="so-panel widget widget_jackrose-sow-events-grid panel-last-child"
                                             data-index="6">
                                            <div class="so-widget-jackrose-sow-events-grid so-widget-jackrose-sow-events-grid-default-d75171398898">
                                                <div class="jackrose-sow-events-grid jackrose-sow-events-grid-2-columns clear">
                                                    <div class="jackrose-sow-events-grid-item jackrose-animation-fade-in-up">
                                                        <div class="jackrose-sow-events-grid-item-wrapper"
                                                             style="background-color: #ffffff">
                                                            <div class="jackrose-sow-events-grid-item-photo"><img
                                                                        width="540" height="270"
                                                                        src="wp-content/uploads/2015/11/event-ceremony-540x270.jpg"
                                                                        class="attachment-grid size-grid" alt=""
                                                                        loading="lazy"
                                                                        sizes="(max-width: 540px) 100vw, 540px"/></div>
                                                            <div class="jackrose-sow-events-grid-item-icon"><img
                                                                        width="60" height="60"
                                                                        src="wp-content/uploads/2015/11/event-icon-ceremony.png"
                                                                        class="attachment-full size-full" alt=""
                                                                        loading="lazy"/></div>
                                                            <h4 class="jackrose-sow-events-grid-item-title typography-heading">
                                                                Lễ Cưới & Vu Quy</h4>
                                                            <div class="jackrose-sow-events-grid-item-description"><p>
                                                                    Đón nhận hồng ân, hạnh phúc trọn vẹn
                                                                </p></div>
                                                            <div class="jackrose-sow-events-grid-item-summary">17h00,
                                                                Thứ sáu, Ngày 09/04/2021 - Tại Thánh đường giáo xứ Thánh
                                                                Tâm (Lộc Tiến), TP. Bảo Lộc, Lâm Đồng

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="jackrose-sow-events-grid-item jackrose-animation-fade-in-up">
                                                        <div class="jackrose-sow-events-grid-item-wrapper"
                                                             style="background-color: #ffffff">
                                                            <div class="jackrose-sow-events-grid-item-photo"><img
                                                                        width="540" height="270"
                                                                        src="wp-content/uploads/2015/11/event-party-540x270.jpg"
                                                                        class="attachment-grid size-grid" alt=""
                                                                        loading="lazy"
                                                                        sizes="(max-width: 540px) 100vw, 540px"/></div>
                                                            <div class="jackrose-sow-events-grid-item-icon"><img
                                                                        width="60" height="60"
                                                                        src="wp-content/uploads/2015/11/event-icon-party.png"
                                                                        class="attachment-full size-full" alt=""
                                                                        loading="lazy"/></div>
                                                            <h4 class="jackrose-sow-events-grid-item-title typography-heading">
                                                                Lễ Tân Hôn</h4>
                                                            <div class="jackrose-sow-events-grid-item-description"><p>
                                                                    Khởi đầu một chương mới, tràn đầy tiếng cười và hạnh
                                                                    phúc</p></div>
                                                            <div class="jackrose-sow-events-grid-item-summary">
                                                                11h30, Ngày 11/04/2021 - Tại 102 Mạc Thị Bưởi, P. Lộc
                                                                Phát, TP. Bảo Lộc, Lâm Đồng
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="pg-7-5" class="panel-grid panel-has-style">
                                <div id="registry" data-jackrose-anchor="registry"
                                     class="panel-row-style panel-row-style-for-7-5">
                                    <div id="pgc-7-5-0" class="panel-grid-cell">
                                        <div id="panel-7-5-0-0"
                                             class="so-panel widget widget_jackrose-sow-section-heading panel-first-child"
                                             data-index="8">
                                            <div class="jackrose-animation-fade-in panel-widget-style panel-widget-style-for-7-5-0-0 jackrose-animate">
                                                <div class="so-widget-jackrose-sow-section-heading so-widget-jackrose-sow-section-heading-default-d75171398898">
                                                    <header class="jackrose-sow-section-header align-center"><h2
                                                                class="jackrose-sow-section-heading typography-section-heading"
                                                                style="color: #dcc8b4">Thanks</h2>
                                                        <p class="jackrose-sow-section-subheading"
                                                           style="color: #888888"></p></header>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="panel-7-5-0-1"
                                             class="so-panel widget widget_sow-editor panel-last-child" data-index="9">
                                            <div class="jr-animation-fade-in-up jackrose-animation-fade-in panel-widget-style panel-widget-style-for-7-5-0-1 jackrose-animate">
                                                <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                    <div class="siteorigin-widget-tinymce textwidget jackrose-sow-quote-text"><p
                                                                style="text-align: center;">
                                                            Sự hiện diện của quý khách là niềm vinh hạnh của gia đình chúng tôi
                                                        </p>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="pg-7-8" class="panel-grid panel-has-style">
                                <div class="siteorigin-panels-stretch panel-row-style panel-row-style-for-7-8"
                                     data-stretch-type="full" id="quote" data-jackrose-anchor="quote"
                                     data-jackrose-background-parallax="{&quot;src&quot;:&quot;https:\/\/singlestroke.io\/demo\/jackrose-wp\/wp-content\/uploads\/2015\/11\/quote.jpg&quot;,&quot;width&quot;:1600,&quot;height&quot;:900,&quot;ratio&quot;:0.5}">
                                    <div id="pgc-7-8-0" class="panel-grid-cell">
                                        <div id="panel-7-8-0-0"
                                             class="so-panel widget widget_jackrose-sow-quote panel-first-child panel-last-child"
                                             data-index="16">
                                            <div class="jackrose-animation-fade-in panel-widget-style panel-widget-style-for-7-8-0-0">
                                                <div class="so-widget-jackrose-sow-quote so-widget-jackrose-sow-quote-default-d75171398898">
                                                    <div class="jackrose-sow-quote align-center"
                                                         data-jackrose-autoplay="5000">
                                                        <div class="jackrose-sow-quote-item">
                                                            <div class="jackrose-sow-quote-text">Đám cưới của chúng tôi chắc chắn sẽ kém vui đi nếu thiếu các bạn! Cảm ơn vì những lời chúc tốt đẹp và sự hiện diện của các bạn. Và cảm ơn vì những điều đặc biệt mà các bạn đã hiện diện trong cuộc sống của chúng tôi. Cảm ơn!
                                                            </div>
                                                        </div>
                                                        <div class="jackrose-sow-quote-item">
                                                            <div class="jackrose-sow-quote-text">
                                                                Và cám ơn rất nhiều!
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="pg-7-9" class="panel-grid panel-has-style">
                                <div id="gallery" data-jackrose-anchor="gallery"
                                     class="panel-row-style panel-row-style-for-7-9">
                                    <div id="pgc-7-9-0" class="panel-grid-cell">
                                        <div id="panel-7-9-0-0"
                                             class="so-panel widget widget_jackrose-sow-section-heading panel-first-child"
                                             data-index="17">
                                            <div class="jackrose-animation-fade-in panel-widget-style panel-widget-style-for-7-9-0-0">
                                                <div class="so-widget-jackrose-sow-section-heading so-widget-jackrose-sow-section-heading-default-d75171398898">
                                                    <header class="jackrose-sow-section-header align-center"><h2
                                                                class="jackrose-sow-section-heading typography-section-heading"
                                                                style="color: #dcc8b4">Captured Moments</h2>
                                                        <p class="jackrose-sow-section-subheading"
                                                           style="color: #888888"></p></header>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="panel-7-9-0-1"
                                             class="so-panel widget widget_jackrose-sow-gallery-grid panel-last-child"
                                             data-index="18">
                                            <div class="jackrose-animation-fade-in-up panel-widget-style panel-widget-style-for-7-9-0-1">
                                                <div class="so-widget-jackrose-sow-gallery-grid so-widget-jackrose-sow-gallery-grid-default-d75171398898">
                                                    <div class="jackrose-sow-gallery-grid">
                                                        <div class="jackrose-sow-gallery-grid-filters"><a href="#"
                                                                                                          class="jackrose-sow-gallery-grid-filter active"
                                                                                                          data-filter="*">All
                                                                photos</a> <a href="#"
                                                                              class="jackrose-sow-gallery-grid-filter"
                                                                              data-filter=".jackrose-sow-engagement">Engagement</a>
                                                            <a href="#" class="jackrose-sow-gallery-grid-filter"
                                                               data-filter=".jackrose-sow-pre-wedding">Pre-Wedding</a>
                                                            <a href="#" class="jackrose-sow-gallery-grid-filter"
                                                               data-filter=".jackrose-sow-withfriends">With Friends</a>
                                                        </div>
                                                        <div class="jackrose-sow-gallery-grid-items jackrose-sow-gallery-grid-4-columns lightgallery clear">
                                                            <?php
                                                            $images = [
                                                                'gallery5.jpg', 'gallery7.jpg', 'gallery3.jpg', 'gallery6.jpg', 'gallery2.jpg',
                                                                'gallery4.jpg', 'gallery8.jpg', 'gallery1.jpg'
                                                            ];
                                                            foreach ($images as $imageUrl) { ?>
                                                                <div class="jackrose-sow-gallery-grid-item jackrose-sow-engagement">
                                                                    <a href="wp-content/uploads/2015/11/<?php echo $imageUrl ?>">
                                                                        <img
                                                                                width="540"
                                                                                src="wp-content/uploads/2015/11/<?php echo $imageUrl ?>"
                                                                                class="attachment-grid size-grid" alt=""
                                                                                loading="lazy"
                                                                                sizes="(max-width: 540px) 100vw, 540px"/>
                                                                    </a>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--                            <div id="pg-7-10" class="panel-grid panel-has-style">-->
                            <!--                                <div class="siteorigin-panels-stretch panel-row-style panel-row-style-for-7-10"-->
                            <!--                                     data-stretch-type="full" id="blog" data-jackrose-anchor="blog">-->
                            <!--                                    <div id="pgc-7-10-0" class="panel-grid-cell">-->
                            <!--                                        <div id="panel-7-10-0-0"-->
                            <!--                                             class="so-panel widget widget_jackrose-sow-section-heading panel-first-child"-->
                            <!--                                             data-index="19">-->
                            <!--                                            <div class="jackrose-animation-fade-in panel-widget-style panel-widget-style-for-7-10-0-0">-->
                            <!--                                                <div class="so-widget-jackrose-sow-section-heading so-widget-jackrose-sow-section-heading-default-d75171398898">-->
                            <!--                                                    <header class="jackrose-sow-section-header align-center"><h2-->
                            <!--                                                            class="jackrose-sow-section-heading typography-section-heading"-->
                            <!--                                                            style="color: #c8b4a0">Our Blog</h2>-->
                            <!--                                                        <p class="jackrose-sow-section-subheading"-->
                            <!--                                                           style="color: #888888"></p></header>-->
                            <!--                                                </div>-->
                            <!--                                            </div>-->
                            <!--                                        </div>-->
                            <!--                                        <div id="panel-7-10-0-1" class="so-panel widget widget_jackrose-sow-posts-grid"-->
                            <!--                                             data-index="20">-->
                            <!--                                            <div class="so-widget-jackrose-sow-posts-grid so-widget-jackrose-sow-posts-grid-default-d75171398898">-->
                            <!--                                                <div class="jackrose-sow-blog-grid jackrose-sow-blog-grid-3-columns clear">-->
                            <!--                                                    <div class="jackrose-sow-blog-post jackrose-animation-fade-in-up"><a-->
                            <!--                                                            class="jackrose-sow-blog-post-thumbnail"-->
                            <!--                                                            href="blog/organizing-wedding-events/index.html"-->
                            <!--                                                            rel="bookmark"> <img width="540" height="398"-->
                            <!--                                                                                 src="wp-content/uploads/2015/11/blog2-540x398.jpg"-->
                            <!--                                                                                 class="attachment-grid size-grid wp-post-image"-->
                            <!--                                                                                 alt="" loading="lazy"-->
                            <!--                                                                                 srcset="https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog2-540x398.jpg 540w, https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog2-300x221.jpg 300w, https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog2-768x565.jpg 768w, https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog2-1024x754.jpg 1024w, https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog2.jpg 1080w"-->
                            <!--                                                                                 sizes="(max-width: 540px) 100vw, 540px"/>-->
                            <!--                                                    </a>-->
                            <!--                                                        <div class="jackrose-sow-blog-post-text"><h4-->
                            <!--                                                                class="jackrose-sow-blog-post-title typography-heading">-->
                            <!--                                                            <a href="blog/organizing-wedding-events/index.html">Organizing-->
                            <!--                                                                Wedding Events</a></h4>-->
                            <!--                                                            <div class="jackrose-sow-blog-post-content"><p>Lorem ipsum-->
                            <!--                                                                dolor sit amet, consectetuer adipiscing elit. Aenean-->
                            <!--                                                                commodo ligula eget dolor. Aenean massa. Cum sociis-->
                            <!--                                                                natoque penatibus et magnis dis parturient montes,-->
                            <!--                                                                nascetur ridiculus mus. Donec quam felis,&hellip;</p>-->
                            <!--                                                            </div>-->
                            <!--                                                        </div>-->
                            <!--                                                    </div>-->
                            <!--                                                    <div class="jackrose-sow-blog-post jackrose-animation-fade-in-up"><a-->
                            <!--                                                            class="jackrose-sow-blog-post-thumbnail"-->
                            <!--                                                            href="blog/designing-our-own-wedding-invitation/index.html"-->
                            <!--                                                            rel="bookmark"> <img width="540" height="398"-->
                            <!--                                                                                 src="wp-content/uploads/2015/11/blog1-540x398.jpg"-->
                            <!--                                                                                 class="attachment-grid size-grid wp-post-image"-->
                            <!--                                                                                 alt="" loading="lazy"-->
                            <!--                                                                                 srcset="https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog1-540x398.jpg 540w, https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog1-300x221.jpg 300w, https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog1-768x565.jpg 768w, https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog1-1024x754.jpg 1024w, https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog1.jpg 1080w"-->
                            <!--                                                                                 sizes="(max-width: 540px) 100vw, 540px"/>-->
                            <!--                                                    </a>-->
                            <!--                                                        <div class="jackrose-sow-blog-post-text"><h4-->
                            <!--                                                                class="jackrose-sow-blog-post-title typography-heading">-->
                            <!--                                                            <a href="blog/designing-our-own-wedding-invitation/index.html">Designing-->
                            <!--                                                                Our Own Wedding Invitation</a></h4>-->
                            <!--                                                            <div class="jackrose-sow-blog-post-content"><p>Lorem ipsum-->
                            <!--                                                                dolor sit amet, consectetuer adipiscing elit. Aenean-->
                            <!--                                                                commodo ligula eget dolor. Aenean massa. Cum sociis-->
                            <!--                                                                natoque penatibus et magnis dis parturient montes,-->
                            <!--                                                                nascetur ridiculus mus. Donec quam felis,&hellip;</p>-->
                            <!--                                                            </div>-->
                            <!--                                                        </div>-->
                            <!--                                                    </div>-->
                            <!--                                                    <div class="jackrose-sow-blog-post jackrose-animation-fade-in-up"><a-->
                            <!--                                                            class="jackrose-sow-blog-post-thumbnail"-->
                            <!--                                                            href="blog/choosing-wedding-venue/index.html"-->
                            <!--                                                            rel="bookmark"> <img width="540" height="398"-->
                            <!--                                                                                 src="wp-content/uploads/2015/11/blog3-540x398.jpg"-->
                            <!--                                                                                 class="attachment-grid size-grid wp-post-image"-->
                            <!--                                                                                 alt="" loading="lazy"-->
                            <!--                                                                                 srcset="https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog3-540x398.jpg 540w, https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog3-300x221.jpg 300w, https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog3-768x565.jpg 768w, https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog3-1024x754.jpg 1024w, https://singlestroke.io/demo/jackrose-wp/wp-content/uploads/2015/11/blog3.jpg 1080w"-->
                            <!--                                                                                 sizes="(max-width: 540px) 100vw, 540px"/>-->
                            <!--                                                    </a>-->
                            <!--                                                        <div class="jackrose-sow-blog-post-text"><h4-->
                            <!--                                                                class="jackrose-sow-blog-post-title typography-heading">-->
                            <!--                                                            <a href="blog/choosing-wedding-venue/index.html">Choosing-->
                            <!--                                                                Wedding Venue</a></h4>-->
                            <!--                                                            <div class="jackrose-sow-blog-post-content"><p>Lorem ipsum-->
                            <!--                                                                dolor sit amet, consectetuer adipiscing elit. Aenean-->
                            <!--                                                                commodo ligula eget dolor. Aenean massa. Cum sociis-->
                            <!--                                                                natoque penatibus et magnis dis parturient montes,-->
                            <!--                                                                nascetur ridiculus mus. Donec quam felis,&hellip;</p>-->
                            <!--                                                            </div>-->
                            <!--                                                        </div>-->
                            <!--                                                    </div>-->
                            <!--                                                </div>-->
                            <!--                                            </div>-->
                            <!--                                        </div>-->
                            <!--                                        <div id="panel-7-10-0-2"-->
                            <!--                                             class="so-panel widget widget_jackrose-sow-button panel-last-child"-->
                            <!--                                             data-index="21">-->
                            <!--                                            <div class="so-widget-jackrose-sow-button so-widget-jackrose-sow-button-default-d75171398898">-->
                            <!--                                                <div class="jackrose-sow-buttons align-center jackrose-animation-fade-in">-->
                            <!--                                                    <a class="jackrose-sow-button button" href="#"> More Blog Posts-->
                            <!--                                                        <span class="fa fa-angle-right"></span> </a></div>-->
                            <!--                                            </div>-->
                            <!--                                        </div>-->
                            <!--                                    </div>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                        </div>
            </div>
            </article></main></div>
    </div>
</div>
<footer id="colophon" class="footer-section site-footer" role="contentinfo">
    <div class="wrapper">
        <div class="footer-logo"><img width="210" height="210" src="wp-content/uploads/2015/11/footer-logo.png"
                                      class="attachment-full size-full" loading="lazy"
                                      sizes="(max-width: 210px) 100vw, 210px"/></div>
    </div>
</footer>
<div id="background-music" class="background-music background-music-bottom-left">
    <button class="background-music-toggle toggle"><span class="fa fa-music"></span></button>
    <div class="background-music-embed">
        <iframe width="100%" height="166" scrolling="no" allow="autoplay" frameborder="no"
                src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/134815445&amp;color=b4d2c8&amp;auto_play=true&amp;hide_related=true&amp;show_comments=false&amp;show_user=true&amp;show_reposts=false"></iframe>
    </div>
</div>
</div>
<style type="text/css"></style>
<style type="text/css"></style>
<style type="text/css"></style>
<style type="text/css"></style>
<style type="text/css"></style>
<style type="text/css"></style>
<style type="text/css"></style>
<style type="text/css"></style>
<style type="text/css"></style>
<style type="text/css"></style>
<style type="text/css"></style>
<style type="text/css"></style>
<script type='text/javascript'
        id='siteorigin-panels-front-styles-js-extra'>var panelsStyles = {"fullContainer": "body"};</script>
<script type="text/javascript">document.body.className = document.body.className.replace("siteorigin-panels-before-js", "");</script>
<script defer src="wp-content/cache/autoptimize/js/autoptimize_e24d16832299ee22b18dc1972656f8a1.js"></script>
</body>
<!-- Mirrored from singlestroke.io/demo/jackrose-wp/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 11 Mar 2021 09:58:29 GMT -->
</html>